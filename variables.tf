variable "lokalizacja" {
  default = "eu-central-1"
  description = "lokalizacja - nazwa regionu AWS"
}

variable "adresacja" {
  default = "10.200.100.0/22"
  description = "adresacja - CIDR dla sieci virtualnej"
}

variable "subnets_cidrs" {
  type = map(any)
  description = "subnets_cidrs - lista parametrów podsieci lokalnych, mapa: CIRD: AZ"
  default = {
    "10.200.101.0/24" = "eu-central-1a",
    "10.200.102.0/24" = "eu-central-1b"
  }
}
variable "subnets_pub_cidrs" {
  description = "subnets_cidrs - lista parametrów podsieci publicznych, mapa:  CIRD: AZ"
  type = map(any)
  default = {
    "10.200.100.0/24" = "eu-central-1a"
  }
}
