adresacja = "10.200.104.0/22"

subnets_pub_cidrs = {
  "10.200.104.0/28" = "eu-central-1a",
}

subnets_cidrs = {
  "10.200.105.0/28" = "eu-central-1a",
  "10.200.106.0/28" = "eu-central-1a",
  "10.200.107.0/28" = "eu-central-1a",
}