terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.51.0"
    }
  }
}


provider "aws" {
  region = "eu-central-1"
}


variable "lokalizacja" {
  default = "eu-central-1"
}

variable "adresacja" {
  default = "10.200.100.0/22"
}

variable "subnets_pub_cidrs" {
  type = map(any)
  default = {
    "10.200.100.0/24" = "eu-central-1a",
  }
}

variable "subnets_cidrs" {
  type = map(any)
  default = {
    "10.200.101.0/24" = "eu-central-1a"
  }
}


module "aws1" {
  source = "git::https://gitlab.com/prze-cze/terraform-aws-mod1?ref=v0.1.1"

  adresacja         = var.adresacja
  subnets_cidrs     = var.subnets_cidrs
  subnets_pub_cidrs = var.subnets_pub_cidrs
  lokalizacja       = var.lokalizacja

}
