output "vpc_id" {
  value = aws_vpc.vpc.id
  description = "VPC ID"
}

output "subnets_ids" {
  value = values(aws_subnet.local)[*].id
  description = "Local Subnets by ID"
}

output "subnets_pub_ids" {
  value = values(aws_subnet.public)[*].id
  description = "Public Subnets by ID"
}
