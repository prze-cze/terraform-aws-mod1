
## VPC and Subnets

resource "aws_vpc" "vpc" {
  cidr_block = var.adresacja
  enable_dns_support = true 
  tags = {
    Name = "aws-net"
  }
}


resource "aws_subnet" "local" {
  for_each                = { for i, cidr in keys(var.subnets_cidrs) : i => cidr }
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = each.value
  availability_zone       = var.subnets_cidrs[each.value]
  map_public_ip_on_launch = false  ## false - default
  tags = {
    Name = "Subnet-${each.key + 1}"
  }
}

resource "aws_subnet" "public" {
  for_each                = { for i, cidr in keys(var.subnets_pub_cidrs) : i => cidr }
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = each.value
  availability_zone       = var.subnets_pub_cidrs[each.value]
  map_public_ip_on_launch = true
  tags = {
    Name = "Subnet-pub-${each.key + 1}"
  }
}


## Route tables

resource "aws_route_table" "local" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "local-rt"
  }
}

resource "aws_route_table_association" "local" {
  count          = length(var.subnets_cidrs)
  subnet_id      = element(values(aws_subnet.local)[*].id, count.index)
  route_table_id = aws_route_table.local.id
}

resource "aws_internet_gateway" "ig" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name        = "local-igw"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_vpc.vpc.main_route_table_id 
  # route_table_id         = data.aws_route_table.public.id
  gateway_id             = aws_internet_gateway.ig.id
  destination_cidr_block = "0.0.0.0/0"
}



## NAT gw for outboud connections from local net

resource "aws_eip" "nat_ip" {
  vpc        = true
  depends_on = [aws_internet_gateway.ig]
}

resource "aws_nat_gateway" "nat-ig" {
  allocation_id = aws_eip.nat_ip.id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name = "test-nat-gw"
  }

  depends_on = [aws_internet_gateway.ig]
}

resource "aws_route" "local-nat-rt" {
  route_table_id         = aws_route_table.local.id
  nat_gateway_id         = aws_nat_gateway.nat-ig.id
  destination_cidr_block = "0.0.0.0/0"
}


